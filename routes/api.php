<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// comment out to allow create route cache

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api'], 'namespace' => 'API'], function () {
    Route::get('user', "UserController@index")->name('api.user');

    // users roles & permissions management
    Route::get('roles-permissions-management',
        "RolesPermissionsManagementController@index")->name('api.roles-permissions-management');
    Route::get('users/{user}/roles', 'UserController@roles')->name('api.user.roles');
    Route::put('users/{user}/assign-roles', 'UserController@assignRoles')->name('api.user.assign-roles');
    Route::put('users/{user}/roles', 'UserController@syncRoles')->name('api.user.sync-roles');
    Route::delete('users/{user}/remove-roles', 'UserController@removeRoles')->name('api.user.remove-roles');
});
