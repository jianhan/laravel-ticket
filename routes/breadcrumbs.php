<?php

// Admin dashboard breadcrumb
Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

// Admin roles and permissions management
Breadcrumbs::register('admin.roles-permissions-management', function ($breadcrumbs) {
    $breadcrumbs->parent("admin.dashboard");
    $breadcrumbs->push('Roles & Permissions', route('admin.roles-permissions-management'));
});
