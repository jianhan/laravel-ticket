<?php

namespace Ticket\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidIDs implements Rule
{

    public function passes($attribute, $value)
    {
        if (!is_array($value) || empty($value)) {
            return false;
        }

        foreach ($value as $v) {
            if (!is_numeric($v) || (int)$v < 1) {
                return false;
            }
        }

        return true;
    }

    public function message()
    {
        return ':attribute must not be empty and all it\'s elements must be integer';
    }
}
