<?php

namespace Ticket\Models;

use Illuminate\Database\Eloquent\Model;
use Ticket\Traits\CommonEloquentScopes;

class PermissionCategory extends Model
{
    use CommonEloquentScopes;
    /**
     * Attributes to be guarded, not allowed for mass update
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
