<?php

namespace Ticket\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Zend\Diactoros\Request;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Ticket\Traits\CommonEloquentScopes;
use Laravel\Passport\HasApiTokens;
use Ticket\Models\Role;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait, CommonEloquentScopes, HasApiTokens;

    /**
     * @var array relations that always eager load
     */
    protected $with = ['roles'];

    /**
     * Attributes to be guarded, not allowed for mass update
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Attributes to be case to carbon date object
     *
     * @var array
     */
    protected $dates = ['date_of_birth'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * Function setPasswordAttribute mutator ensure that password always hashed.
     *
     * @param string $password
     *
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * @param array $roles
     */
    public function assignRoles(array $roles)
    {
        $this->attachRoles(Role::whereIn('id', $roles)->get());
    }
}
