<?php namespace Ticket\Models;

use Zizaco\Entrust\EntrustRole;
use Ticket\Traits\CommonEloquentScopes;

class Role extends EntrustRole
{
    use CommonEloquentScopes;

    /**
     * @var array relations that always eager load
     */
    protected $with = ['perms'];
}
