<?php namespace Ticket\Models;

use Zizaco\Entrust\EntrustPermission;
use Ticket\Traits\CommonEloquentScopes;

class Permission extends EntrustPermission
{
    use CommonEloquentScopes;

    public function category()
    {
        return $this->belongsTo(PermissionCategory::class);
    }
}
