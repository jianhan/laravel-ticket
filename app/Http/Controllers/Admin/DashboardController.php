<?php

namespace Ticket\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticket\Http\Controllers\Controller;

/**
 * Admin dashboard controller
 */
class DashboardController extends Controller
{

    /**
     * Index action renders view for dashboard index page.
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $this->autoRender();
    }
}
