<?php

namespace Ticket\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticket\Http\Controllers\Controller;
use Ticket\Models\Role;
use Ticket\Models\Permission;

class RolesPermissionsManagementController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::with('perms')->orderBy("display_name")->get();
        $permissions = Permission::orderBy('display_name')->get();
        return $this->autoRender(get_defined_vars());
    }
}
