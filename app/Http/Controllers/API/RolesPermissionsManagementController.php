<?php

namespace Ticket\Http\Controllers\API;

use Illuminate\Http\Request;
use Ticket\Http\Controllers\Controller;
use Ticket\Models\Role;
use Ticket\Models\User;
use Ticket\Models\Permission;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class RolesPermissionsManagementController generates backend api will be used
 * for update roles , permissions and related users.
 *
 * @package Ticket\Http\Controllers\API
 */
class RolesPermissionsManagementController extends BaseController
{
    /**
     *  Index action return json object will be used by roles & permissions
     *  management view.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $users = User::orderBy('name')->get();
        $permissions = Permission::orderBy('display_name')->get();
        $roles = Role::orderBy('display_name')->get();
        return  response()->json([
            'users' => $users,
            'permissions' => $permissions,
            'roles' => $roles
        ]);
    }
}
