<?php

namespace Ticket\Http\Controllers\API;

use Illuminate\Http\Request;
use Ticket\Http\Controllers\Controller;
use Ticket\Http\Controllers\APIController;
use Ticket\Models\User;
use Ticket\Models\Role;
use Ticket\Rules\ValidIDs;

class UserController extends APIController
{
    public function roles(Request $request, User $user)
    {
        return response()->json($user->roles);
    }

    public function assignRoles(Request $request, User $user)
    {
        $request->validate([
            'roles' => ['required', new ValidIDs()]
        ]);

        $roles = $request->get('roles');
        $user->assignRoles($roles);
        return response()->json([
            'status' => 'success',
            'message' => sprintf("%d roles has been assigned to user %s", $user->name, count($roles))
        ]);
    }
}
