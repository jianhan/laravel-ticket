<?php

/**
 * @param string $routeName name of input route.
 *
 * @return string active class name of link.
 */
function hightLightMenuClass(string $routeName) : string
{
    if (\Route::currentRouteName() == $routeName) {
        return 'active';
    }

    return "";

}
