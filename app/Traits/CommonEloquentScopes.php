<?php namespace Ticket\Traits;

trait CommonEloquentScopes
{
    public function scopeBy($query, string $field, string $value, string $operator = '=')
    {
        return $query->where($field, $operator, $value);
    }

}
