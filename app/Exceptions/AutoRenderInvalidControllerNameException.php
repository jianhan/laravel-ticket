<?php

namespace Ticket\Exceptions;

use Exception;

class AutoRenderInvalidControllerNameException extends Exception
{

    public static function invalidControllerName(string $controllerName) : AutoRenderInvalidControllerNameException
    {
        return new static("Controller name must end with 'Controller', '$controllerName' found");
    }
}
