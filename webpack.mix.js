const {
    mix
} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.webpackConfig({
    module: {
        loaders: [{
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]

    }
})


mix.js('resources/assets/js/app.js', 'public/js')
    .styles([
        'resources/assets/css/bootstrap.min.css',
        'resources/assets/css/oneui.min.css',
    ], 'public/css/app.css')
    .mix.copy('resources/assets/fonts', 'public/fonts')
    .mix.copy('resources/assets/img', 'public/assets/img');
