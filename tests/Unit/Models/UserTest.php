<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Ticket\Models\User;
use Hash;

class UserTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    public function testUserModelCanSetPasswordAsHash()
    {
        $user = User::create([
            'name' => 'test',
            'email' => 'test@email.com',
            'password' => 'password',
            'remember_token' => str_random(10),
        ]);

        $this->assertTrue(Hash::check('password', $user->password));
    }

    public function testUserAssignRoles()
    {

    }
}
