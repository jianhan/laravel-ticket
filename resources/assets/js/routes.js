import LoginPage from './components/pages/LoginPage.vue'

const routes = [
    {path: '/login', name: 'login-page', component: LoginPage},
]

export default routes