<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-8">
            <h1 class="page-heading">
                {{ $title  }}@if(isset($subTitle) && !empty($subTitle))<small>{{$subTitle}}.</small>@endif
            </h1>
        </div>
        @if(isset($breadcrumb) && !empty($breadcrumb))
            <div class="col-sm-4 text-right hidden-xs">
                {!! Breadcrumbs::render($breadcrumb) !!}
            </div>
        @endif
    </div>
</div>