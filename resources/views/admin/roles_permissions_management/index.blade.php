@extends("layouts.admin")

@section("content")
    @if($roles->isEmpty() || $permissions->isEmpty())
        <div class="alert alert-warning">
            <p>No roles or permissions available yet, please add role or permissions to the system</p>
        </div>
    @else
        <div class="row">
            @foreach($roles as $role)
                <div class="col-sm-6 col-lg-3 col-md-4">
                    <div class="block block-themed">
                        <div class="block-header bg-gray">
                            <h3 class="block-title">{{$role->display_name}}</h3>
                        </div>
                        <div class="block-content">
                            <table class="table table-condensed">
                                @foreach($permissions as $permission )
                                        <tr>
                                            <td>{{$permission->display_name}}</td>
                                        </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@stop

@section("page_header")
    @include("layouts.partials.admin_page_header", ["title" => "Roles & Permissions", "breadcrumb" => "admin.roles-permissions-management"])
@stop