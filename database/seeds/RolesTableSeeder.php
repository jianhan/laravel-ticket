<?php

use Illuminate\Database\Seeder;
use Ticket\Models\Role;
use Ticket\Models\Permission;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data array
        $data = [
            [
                'name' => 'owner',
                'display_name' => 'Project Owner',
                'description' => 'Role represent the owner of the project',
                'all_permissions' => true,
            ],
            [
                'name' => 'super-user',
                'display_name' => 'Super User',
                'description' => 'Role represent super user',
                'all_permissions' => true,
            ],
            [
                'name' => 'administrator',
                'display_name' => 'Administrator',
                'description' => 'Role represent administrator',
            ],
            [
                'name' => 'site-manager',
                'display_name' => 'Site Manager',
                'description' => 'Role represent site manager'
            ],
            [
                'name' => 'customer',
                'display_name' => 'Customer',
                'description' => 'Role represent customer'
            ],
        ];

        foreach ($data as $k => $v) {
            $allPermissions = isset($v['all_permissions']) ? $v['all_permissions'] : false;
            unset($v['all_permissions']);
            $role = Role::create($v);
            if ($allPermissions === true) {
                $role->attachPermissions(Permission::all());
            }
        }
    }
}
