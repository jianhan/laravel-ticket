<?php

use Illuminate\Database\Seeder;
use Ticket\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data array
        $data = [
            // tickets related permissions
            [
                'name' => 'view-tickets',
                'display_name' => 'View Tickets',
                'description' => 'Permission to view tickets',
            ],
            [
                'name' => 'create-ticket',
                'display_name' => 'Create Ticket',
                'description' => 'Permission to create ticket'
            ],
            [
                'name' => 'edit-ticket',
                'display_name' => 'Edit Ticket',
                'description' => 'Permission to edit ticket'
            ],
            [
                'name' => 'delete-ticket',
                'display_name' => 'Delete Ticket',
                'description' => 'Permission to delete ticket'
            ],
            // ticket categories related permission
            [
                'name' => 'view-ticket-categories',
                'display_name' => 'View Ticket Categories',
                'description' => 'Permission to view ticket categories'
            ],
            [
                'name' => 'create-ticket-category',
                'display_name' => 'Create Ticket Category',
                'description' => 'Permission to create ticket category'
            ],
            [
                'name' => 'edit-ticket-category',
                'display_name' => 'Edit Ticket Category',
                'description' => 'Permission to edit ticket category'
            ],
            [
                'name' => 'delete-ticket-category',
                'display_name' => 'Delete Ticket Category',
                'description' => 'Permission to delete ticket category'
            ],
            // users related permission
            [
                'name' => 'view-users',
                'display_name' => 'View Users',
                'description' => 'Permission to view users'
            ],
            [
                'name' => 'create-user',
                'display_name' => 'Create User',
                'description' => 'Permission to create user'
            ],
            [
                'name' => 'edit-user',
                'display_name' => 'Edit User',
                'description' => 'Permission to edit user'
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Delete User',
                'description' => 'Permission to delete user'
            ],
            // permissions to manage roles & permissions
            [
                'name' => 'manage-roles-permissions',
                'display_name' => 'Manage Roles & Permissions',
                'description' => 'Permission manage roles and permissions'
            ],
        ];

        foreach ($data as $k => $v) {
            $owner = new Permission();
            $owner->create($v);
        }
    }
}
