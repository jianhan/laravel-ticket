<?php

use Illuminate\Database\Seeder;
use \Ticket\Models\PermissionCategory;

class PermissionCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // hard code ID is fine in this case, since this is only used for setting up the application
        $data = [
            [
                'id' => 1,
                'name' => 'Users',
                'description' => 'User related permissions',
                'order' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Tickets',
                'description' => 'Tickets related permissions',
                'order' => 2,
            ],
            [
                'id' => 3,
                'name' => 'Ticket Categories',
                'description' => 'Ticket categories related permissions',
                'order' => 3,
            ],
            [
                'id' => 4,
                'name' => 'Roles & Permissions',
                'description' => 'Permissions to manage roles and permissions',
                'order' => 3,
            ],
        ];

        foreach ($data as $k => $v) {
            PermissionCategory::created($v);
        }
    }
}
